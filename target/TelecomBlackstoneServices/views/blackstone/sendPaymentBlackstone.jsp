<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>

<head>
<title>Purchase Verification</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="expires" content="-1" />
<meta name="HandheldFriendly" content="true" />
<meta name="viewport" content="width=device-width" />
</script>
<style type="text/css">
root {
	display: block;
}

html {
	margin: 0;
	font-size: 62.5%;
	color: white;
	background-color: #D3441C;
}
body {
	font-size: 14px;
    font-weight: bold;
	line-height: 1.428571429;
}

@media ( min-width :750px) {
	.container {
		width: 100%;
		margin-right: auto;
		margin-left: auto;
	}
}

@media ( min-width :992px) {
	.container {
		width: 700px;
		margin-right: auto;
		margin-left: auto;
	}
} 
</style>
</head>
<body style="width: 100%">
	<form method="post" name="form1" autocomplete="off"
		action="https://services.bmspay.com/testing/api/Transactions/Sale"
		enctype="application/x-www-form-urlencoded">
		<input type="hidden" name="AppKey" value="${appKey}" />
		<input type="hidden" name="AppType" value="${appType}" />
		<input type="hidden" name="mid" value="${mid}" />
		<input type="hidden" name="cid" value="${cid}" />
		<input type="hidden" name="UserName" value="${bsUser}" />
		<input type="hidden" name="Password" value="${bkPassword}" />
		<input type="hidden" name="Amount" value="${montoTotal}" />
		<input type="hidden" name="CardNumber" value="${tarjeta}" />
		<input TYPE="hidden" NAME="ExpDate" VALUE="${vigencia}"> 
		<input TYPE="hidden" NAME="NameOnCard" VALUE="${nombre}"> 
		<input TYPE="hidden" NAME="CVN" VALUE="${cvv2}">
		<input TYPE="hidden" NAME="UserTransactionNumber" VALUE="${idTransaccion}">
		<input TYPE="hidden" NAME="TransactionType" VALUE="${tipo}">
	</form>
	<div class="container">
		
		<br />
		<table border="0" width="100%">
			<thead>
			<tr>
				<th><p>Envio de informaci&oacute;n.</p></th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>
					<p><br/>Se est&aacute; enviando la informaci&oacute;n, favor de esperar. <br/>
					</p>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
	<script type="text/javascript">
		document.form1.submit();
	</script>
</body>
</html>
