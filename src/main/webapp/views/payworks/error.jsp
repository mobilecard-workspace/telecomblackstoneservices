<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Error Pago</title>
	
	<style type="text/css">
        root { 
            display: block;
        }

        html
        {
        	margin: 0;
            font-family: arial;
            font-size: 12px;
            font-weight: bold;
            color: white;
            background-color: #D3441C;
        }

        td{
        	font-family: arial;
            font-size: 12px;
        }

        .title
        {
        	font-family: arial;
        	font-weight: bold;
            font-size: 14px;

        }

        .title2
        {
        	font-family: arial;
            font-size: 12px;
        }
    </style>  
</head>
<body>
	<center>
	<table cellpadding="1" cellspacing="1">
		<tr>
			<td align="center">
				<h1>Error al procesar pago</h1>
			</td>
		</tr>
		<tr>
			<td >
				<h3>No es posible procesar su transaccion</h3>
			</td>
		</tr>
		<tr>
			<td >
				<strong>Codigo Error:</strong>
			</td>
		</tr>
		<tr>
			<td >
				<strong>${error}</strong>
			</td>
		</tr>
	</table>
	</center>
</body>
</html>