package com.addcel.telecom.usa.blackstone.services.model.vo;

public class BlackstoneDataConnection {

	private String UserName;
		
	private String Password;
	
	private String mid; 
	
	private String cid;
	
	private String TransactionType;
	
	private String reversalid;
	
	private String AppKey;
	
	private String AppType;
	
	private String HostUserName;
	
	private String HostPassword;
	
	private String endPoint;

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getTransactionType() {
		return TransactionType;
	}

	public void setTransactionType(String transactionType) {
		TransactionType = transactionType;
	}

	public String getReversalid() {
		return reversalid;
	}

	public void setReversalid(String reversalid) {
		this.reversalid = reversalid;
	}

	public String getAppKey() {
		return AppKey;
	}

	public void setAppKey(String appKey) {
		AppKey = appKey;
	}

	public String getAppType() {
		return AppType;
	}

	public void setAppType(String appType) {
		AppType = appType;
	}

	public String getHostUserName() {
		return HostUserName;
	}

	public void setHostUserName(String hostUserName) {
		HostUserName = hostUserName;
	}

	public String getHostPassword() {
		return HostPassword;
	}

	public void setHostPassword(String hostPassword) {
		HostPassword = hostPassword;
	}

	public String getEndPoint() {
		return endPoint;
	}

	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}
	
}
