package com.addcel.telecom.usa.blackstone.services.model.vo;

public class BlackstonePaymentRequest {

	private String UserName;
	
	private String testing_user;
	
	private String Password;
	
	private String mid; 
	
	private String cid;
	
	private String Amount;
	
	private String TransactionType;
	
	private String Track2;
	
	private String ZipCode;
	
	private String CVN; 
	
	private String CardNumber;
	
	private String ExpDate;
	
	private String NameOnCard;
	
	private String reversalid;
	
	private String AppKey;
	
	private String AppType;
	
	private String HostUserName;
	
	private String HostPassword;
	
	private String UserTransactionNumber;

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getTesting_user() {
		return testing_user;
	}

	public void setTesting_user(String testing_user) {
		this.testing_user = testing_user;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getAmount() {
		return Amount;
	}

	public void setAmount(String amount) {
		Amount = amount;
	}

	public String getTransactionType() {
		return TransactionType;
	}

	public void setTransactionType(String transactionType) {
		TransactionType = transactionType;
	}

	public String getTrack2() {
		return Track2;
	}

	public void setTrack2(String track2) {
		Track2 = track2;
	}

	public String getZipCode() {
		return ZipCode;
	}

	public void setZipCode(String zipCode) {
		ZipCode = zipCode;
	}

	public String getCVN() {
		return CVN;
	}

	public void setCVN(String cVN) {
		CVN = cVN;
	}

	public String getCardNumber() {
		return CardNumber;
	}

	public void setCardNumber(String cardNumber) {
		CardNumber = cardNumber;
	}

	public String getExpDate() {
		return ExpDate;
	}

	public void setExpDate(String expDate) {
		ExpDate = expDate;
	}

	public String getNameOnCard() {
		return NameOnCard;
	}

	public void setNameOnCard(String nameOnCard) {
		NameOnCard = nameOnCard;
	}

	public String getReversalid() {
		return reversalid;
	}

	public void setReversalid(String reversalid) {
		this.reversalid = reversalid;
	}

	public String getAppKey() {
		return AppKey;
	}

	public void setAppKey(String appKey) {
		AppKey = appKey;
	}

	public String getAppType() {
		return AppType;
	}

	public void setAppType(String appType) {
		AppType = appType;
	}

	public String getHostUserName() {
		return HostUserName;
	}

	public void setHostUserName(String hostUserName) {
		HostUserName = hostUserName;
	}

	public String getHostPassword() {
		return HostPassword;
	}

	public void setHostPassword(String hostPassword) {
		HostPassword = hostPassword;
	}

	public String getUserTransactionNumber() {
		return UserTransactionNumber;
	}

	public void setUserTransactionNumber(String userTransactionNumber) {
		UserTransactionNumber = userTransactionNumber;
	}

}