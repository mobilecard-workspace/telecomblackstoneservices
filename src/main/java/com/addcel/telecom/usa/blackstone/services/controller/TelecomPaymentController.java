package com.addcel.telecom.usa.blackstone.services.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.telecom.usa.blackstone.services.services.PaymentService;

@Controller
public class TelecomPaymentController {
	
	@Autowired
	private PaymentService payService;
	
	private static final String VIEW_HOME = "home";
	
	private static final String PATH_TEST = "/test";
	
	@RequestMapping(value = PATH_TEST, method=RequestMethod.GET)
	public ModelAndView home() {	
		ModelAndView mav = new ModelAndView(VIEW_HOME);
		return mav;	
	}
	
	@RequestMapping(value = "/telecom/blackstone/send/payment", method=RequestMethod.POST)
	public @ResponseBody String registraTransaccion(@RequestParam("json") String json, HttpSession session) {
		return payService.autorizacionBlackStone(json, session);
	}	
}
