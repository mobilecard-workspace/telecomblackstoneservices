package com.addcel.telecom.usa.blackstone.services.model.vo;

public class PaymentPlanInfo {

	private int PlanId;
	
	private double SwipeDiscount;
	
	private double SwipeTransactionFee;
	
	private double NonSwipeDiscount;
	
	private double NonSwipeTransactionFee;
	
	private double MonthlyFee;
	
	private String Description;
	
	private String PlanName;
	
	private boolean DisplayName;

	public int getPlanId() {
		return PlanId;
	}

	public void setPlanId(int planId) {
		PlanId = planId;
	}

	public double getSwipeDiscount() {
		return SwipeDiscount;
	}

	public void setSwipeDiscount(double swipeDiscount) {
		SwipeDiscount = swipeDiscount;
	}

	public double getSwipeTransactionFee() {
		return SwipeTransactionFee;
	}

	public void setSwipeTransactionFee(double swipeTransactionFee) {
		SwipeTransactionFee = swipeTransactionFee;
	}

	public double getNonSwipeDiscount() {
		return NonSwipeDiscount;
	}

	public void setNonSwipeDiscount(double nonSwipeDiscount) {
		NonSwipeDiscount = nonSwipeDiscount;
	}

	public double getNonSwipeTransactionFee() {
		return NonSwipeTransactionFee;
	}

	public void setNonSwipeTransactionFee(double nonSwipeTransactionFee) {
		NonSwipeTransactionFee = nonSwipeTransactionFee;
	}

	public double getMonthlyFee() {
		return MonthlyFee;
	}

	public void setMonthlyFee(double monthlyFee) {
		MonthlyFee = monthlyFee;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getPlanName() {
		return PlanName;
	}

	public void setPlanName(String planName) {
		PlanName = planName;
	}

	public boolean isDisplayName() {
		return DisplayName;
	}

	public void setDisplayName(boolean displayName) {
		DisplayName = displayName;
	}
	
}
