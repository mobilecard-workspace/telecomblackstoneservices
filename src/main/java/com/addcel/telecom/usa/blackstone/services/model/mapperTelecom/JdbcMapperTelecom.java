package com.addcel.telecom.usa.blackstone.services.model.mapperTelecom;

import org.apache.ibatis.annotations.Param;

import com.addcel.telecom.usa.blackstone.services.model.vo.Bitacora;
import com.addcel.telecom.usa.blackstone.services.model.vo.BlackstoneDataConnection;
import com.addcel.telecom.usa.blackstone.services.model.vo.BlackstoneResponse;
import com.addcel.telecom.usa.blackstone.services.model.vo.Usuario;

public interface JdbcMapperTelecom {

	public Usuario getUsuario(@Param(value = "id") long idUsuario);

	public BlackstoneDataConnection getBlackstoneData(String string);
	
	public void insertaBitacora(Bitacora bitacora);
	
	public void actualizaBitacora(Bitacora response);
	
	public void actualizaBitacoraBlackstone(BlackstoneResponse response);
	
	public void insertaBitacoraBlackstone(BlackstoneResponse response);	
}
