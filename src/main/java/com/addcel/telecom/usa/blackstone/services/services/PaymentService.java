package com.addcel.telecom.usa.blackstone.services.services;


import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.addcel.telecom.usa.blackstone.services.model.mapper.JdbcMapper;
import com.addcel.telecom.usa.blackstone.services.model.mapperTelecom.JdbcMapperTelecom;
import com.addcel.telecom.usa.blackstone.services.model.vo.Bitacora;
import com.addcel.telecom.usa.blackstone.services.model.vo.BlackstoneDataConnection;
import com.addcel.telecom.usa.blackstone.services.model.vo.BlackstonePaymentRequest;
import com.addcel.telecom.usa.blackstone.services.model.vo.BlackstoneResponse;
import com.addcel.telecom.usa.blackstone.services.model.vo.PaymentRequest;
import com.addcel.telecom.usa.blackstone.services.model.vo.PaymentResponse;
import com.addcel.telecom.usa.blackstone.services.model.vo.Usuario;
import com.addcel.telecom.usa.blackstone.services.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;

@Service
public class PaymentService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentService.class);
	
	public static final String VIEW_START_PAYMENT_FORM = "blackstone/paymentForm";
	
	public static final String VIEW_BLACKSTONE_RESPONSE = "blackstone/blackstoneResponse";
	
	private static Gson GSON = new Gson();
	
	@Autowired
	private JdbcMapper mapper;
	
	@Autowired
	private JdbcMapperTelecom mapperTelecom;
	
	public String autorizacionBlackStone(String json, HttpSession session){
		PaymentRequest pago = null;
		PaymentRequest pagoTlc = null;
		Usuario usuario = null;
		BlackstoneResponse response = null;
		BlackstoneDataConnection conn = null;
		try {
			LOGGER.info("[BLACKSTONE] json: "+json);
			pago = (PaymentRequest) GSON.fromJson(json, PaymentRequest.class);	
			pagoTlc = (PaymentRequest) GSON.fromJson(json, PaymentRequest.class);
			LOGGER.info("[BLACKSTONE] - INICIO SOLICITUD PAGO: "+pago.toTrace());
			conn = mapperTelecom.getBlackstoneData("1");
			insertaBitacoraMobilecard(pago);
			insertaBitacoraTelecom(pagoTlc);
			json  = blackstoneJsonRequest(conn, pago);
			response = sendPay2Blackstone(conn.getEndPoint(), json, String.valueOf(pagoTlc.getIdTransaccion()));
			actualizaBitacoraMobilecard(pago, response);
			actualizaBitacoraTelecom(pagoTlc, response);
			json = jsonRespuesta(response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}
	
	private String jsonRespuesta(BlackstoneResponse blackstoneResp){
		PaymentResponse response = new PaymentResponse();
		try {
			if(blackstoneResp.getResponseCode() != 200){
				response.setErrorCode(-1);
			} else {
				response.setAuthorization(blackstoneResp.getAuthorizationNumber());
				response.setReference(blackstoneResp.getServiceReferenceNumber());
			}
			response.setMessage(blackstoneResp.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return GSON.toJson(response);
	}
	
	private BlackstoneResponse sendPay2Blackstone(String url, String json, String idTransaccionTlc){
		BlackstoneResponse response = null;
		try {
			LOGGER.info("[BLACKSTONE] -[blackstoneJsonRequest]: ");
			RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
        	headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        	HttpEntity<String> request = new HttpEntity<String>(json, headers);
			String respNotificacion = restTemplate.postForObject(url, request, String.class);
			response = GSON.fromJson(respNotificacion, BlackstoneResponse.class);			
			response.setUserTransactionNumber(idTransaccionTlc);
			response.setMessage(response.getMsg()[0]);
			LOGGER.info("RESPUESTA BLACKSTONE: "+respNotificacion);
			mapperTelecom.insertaBitacoraBlackstone(response);
		} catch (Exception e) {	
			e.printStackTrace();
		}
		return response;
	}
		

	private String blackstoneJsonRequest(BlackstoneDataConnection connection, PaymentRequest pago) {
		BlackstonePaymentRequest req = new BlackstonePaymentRequest();
		try {
			LOGGER.info("[BLACKSTONE] -[blackstoneJsonRequest]: ");
			BeanUtils.copyProperties(connection, req);
			req.setAmount(String.valueOf(pago.getMonto()));
			req.setCardNumber(pago.getCardNumber());
			req.setExpDate(pago.getExpDate());
			req.setCVN(pago.getCvv2());
			req.setUserTransactionNumber(String.valueOf(pago.getIdTransaccion()));
			req.setZipCode(pago.getZipCode());
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return GSON.toJson(req);
	}
	
	private void actualizaBitacoraMobilecard(PaymentRequest pago, BlackstoneResponse response) {
		Bitacora bitacora = null;
		try {
			LOGGER.info("[BLACKSTONE] -[actualizaBitacora]: ");
			bitacora = new Bitacora();
			bitacora.setCodigoError(response.getResponseCode());
			bitacora.setStatus(0);
			bitacora.setTicket(response.getMessage());
			bitacora.setIdTransaccion(pago.getIdTransaccion());			
			if (response.getResponseCode() == 200)	{
				/*este es el codigo de exito que se va setear en t_bitacora
				 cuando las transacciones de telecom son exitosas y las guardo en la BD de mobileCard* */
				bitacora.setBitStatus(99);	
				bitacora.setBitCodigoError(-99);
			}
			else{
				bitacora.setBitStatus(0);	
				bitacora.setBitCodigoError(response.getResponseCode());
			}
			
			mapper.actualizaBitacora(bitacora);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	private void insertaBitacoraMobilecard(PaymentRequest pago){
		Bitacora bitacora = null;
		try {
			LOGGER.info("[BLACKSTONE] -[insertaBitacora]: ");
			bitacora = new Bitacora();
			bitacora.setIdUsuario(pago.getIdUsuario());
			bitacora.setIdProveedor(pago.getIdProveedor());
			bitacora.setIdProducto(pago.getIdProducto());
			bitacora.setConcepto(pago.getConcepto());
			bitacora.setMonto(pago.getMonto());
			bitacora.setImei(pago.getImei());
			bitacora.setDestino("");			
//			bitacora.setTarjetaCompra(AddcelCrypto.encryptHard(pago.getCardNumber()));
			bitacora.setTarjetaCompra(UtilsService.setSMS(pago.getCardNumber()));
			bitacora.setTipo(pago.getTipo());
			bitacora.setSoftware(pago.getSoftware());
			bitacora.setModelo(pago.getModelo());
			bitacora.setWkey(pago.getWkey());
			bitacora.setIdAplicacion(14);
			mapper.insertaBitacora(bitacora);
			pago.setIdTransaccion(bitacora.getIdTransaccion());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	private void insertaBitacoraTelecom(PaymentRequest pagoTlc){
		Bitacora bitacora = null;
		try {
			LOGGER.info("[BLACKSTONE] -[insertaBitacora]: ");
			bitacora = new Bitacora();
			bitacora.setIdUsuario(pagoTlc.getIdUsuario());
			bitacora.setIdProveedor(pagoTlc.getIdProveedor());
			bitacora.setIdProducto(pagoTlc.getIdProducto());
			bitacora.setConcepto(pagoTlc.getConcepto());
			bitacora.setMonto(pagoTlc.getMonto());
			bitacora.setImei(pagoTlc.getImei());
			bitacora.setDestino("");
//			bitacora.setTarjetaCompra(AddcelCrypto.encryptHard(pagoTlc.getCardNumber()));
			bitacora.setTarjetaCompra(UtilsService.setSMS(pagoTlc.getCardNumber()));			
			bitacora.setTipo(pagoTlc.getTipo());
			bitacora.setSoftware(pagoTlc.getSoftware());
			bitacora.setModelo(pagoTlc.getModelo());
			bitacora.setWkey(pagoTlc.getWkey());			
			mapperTelecom.insertaBitacora(bitacora);
			pagoTlc.setIdTransaccion(bitacora.getIdTransaccion());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void actualizaBitacoraTelecom(PaymentRequest pagoTlc, BlackstoneResponse response) {
		Bitacora bitacora = null;
		try {
			bitacora = new Bitacora();
			bitacora.setCodigoError(response.getResponseCode());
			bitacora.setStatus(0);
			bitacora.setTicket(response.getMessage());
			bitacora.setIdTransaccion(pagoTlc.getIdTransaccion());
			if (response.getResponseCode() == 200)	{
				
				bitacora.setBitStatus(1);	
				bitacora.setBitCodigoError(response.getResponseCode());
			}
			else{
				bitacora.setBitStatus(0);	
				bitacora.setBitCodigoError(response.getResponseCode());
			}
			mapperTelecom.actualizaBitacora(bitacora);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String desencriptar(String json, HttpSession session)
	{
		
		Usuario usuario = null;
		usuario = (Usuario) GSON.fromJson(json, Usuario.class);
		String num_tarjeta = AddcelCrypto.decryptTarjeta(usuario.getNumTarjeta());
		LOGGER.info("num_tarjeta desencriptada "+num_tarjeta);
		return num_tarjeta;
	}
		
}